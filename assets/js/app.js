$(document).ready(function () {
  $(".product-carousel").slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 5,
    lazyLoad: "ondemand",
    responsive: [
      {
        breakpoint: 1365,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 5,
        },
      },
      {
        breakpoint: 1292,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        },
      },
      {
        breakpoint: 705,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 568,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        },
      },
    ],
  });
});

var productListModel = [];

var discount = function (price, oldPrice) {
  if (oldPrice) var discount = Math.round(100 - (price / oldPrice) * 100);
  return discount;
};

products.forEach(function (product) {
  productListModel.push(
    new productListViewModel(
      product.productId,
      product.name,
      product.url,
      product.image,
      product.price,
      product.oldPrice,
      product.params.basePrice,
      product.params.likeCount,
      product.categories,
      product.inStock,
      product.params.isNew,
      discount(product.price, product.oldPrice)
    )
  );
});

Object.defineProperty(Array.prototype, "chunk", {
  value: function (chunkSize) {
    var R = [];
    for (var i = 0; i < this.length; i += chunkSize)
      R.push(this.slice(i, i + chunkSize));
    return R;
  },
});

var productList = productListModel.chunk(10);
console.log(productList);

var getData = function (size) {
  var i = 0;
  while (i < size) {
    productList[i].forEach(function (product) {
      var productItemHtml = `<div class="item">
      <div class="badges">
        ${
          product.inStock == true
            ? '<span class="instock" title="In Stock"><img data-lazy="/assets/images/instock.svg"></span>'
            : '<span class="outofstock" title"Out of Stock"><img data-lazy="/assets/images/outofstock.svg"></span>'
        }
        ${product.isNew ? '<span class="isnew">NEU</span>' : ""}
        ${
          product.discount
            ? '<span class="discount">-' + product.discount + "%</span>"
            : ""
        }
        ${
          product.likeCount
            ? '<span class="likecount"><img data-lazy="/assets/images/like.svg"> ' +
              product.likeCount +
              "</span>"
            : ""
        }
      </div>
      <div class="img"><a href="${product.url}"><img data-lazy="${
        product.image
      }" alt="${product.name}"></a></div>
      <h3><a href="${product.url}">${product.name}</a></h3>
      <div class="categories">
          <span>${product.categories.join(" | ")}</span>
      </div>
      <div class="price">
          <span class="item-price ${!product.oldPrice && "current"}">${
        product.price
      } €*</span>
          ${
            product.oldPrice
              ? '<span class="old-price">' + product.oldPrice + " €*</span>"
              : ""
          }
          <span class="base-price">${product.basePrice}</span>
      </div>
    </div>`;

      $(".product-carousel#products" + i).append(productItemHtml);
    });

    i++;
  }
};

getData(3);
