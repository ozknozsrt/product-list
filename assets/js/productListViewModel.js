class productListViewModel {
  constructor(
    id,
    name,
    url,
    image,
    price,
    oldPrice,
    basePrice,
    likeCount,
    categories,
    inStock,
    isNew,
    discount
  ) {
    var self = this;

    self.id = id;
    self.name = name;
    self.url = url;
    self.image = image;
    self.price = price;
    self.oldPrice = oldPrice;
    self.basePrice = basePrice;
    self.likeCount = likeCount;
    self.categories = categories;
    self.inStock = inStock;
    self.isNew = isNew;
    self.discount = discount;
  }
}
